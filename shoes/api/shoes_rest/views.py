from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from shoes_rest.models import Shoe, BinVO
from common.json import ModelEncoder



class BinVODetailEncoder(ModelEncoder):
    model=BinVO
    properties={
        "closet_name",
        "import_href",
        "id",
        "bin_number",
        "bin_size",
    }


class ShoeListEncoder(ModelEncoder):
    model=Shoe
    properties=[
        "model_name",
        "bin",
        "id",
        "color",
        "picture_url",
        "manufacturer"
    ]
    encoders={
        "bin":BinVODetailEncoder()
    }
class ShoeDetailEncoder(ModelEncoder):
    model= Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "id",
        "picture_url",
        "bin",
    ]
    encoders= {
        "bin":BinVODetailEncoder(),
    }




@require_http_methods(["GET","POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method =="GET":
        if bin_vo_id is not None:
            shoes=Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes=Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content =json.loads(request.body)
        try:
            bin_href=content["bin"]
            bin=BinVO.objects.get(import_href=bin_href)
            content["bin"]= bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe= Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )



@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoes(request, id):
    if request.method =="GET":
        try:
            shoe= Shoe.objects.get(id=id)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response=JsonResponse({"message": "Shoe does not exist"})
            response.status_code=404
            return response
    elif request.method== "DELETE":
        try:
            count,_=Shoe.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count>0})
        except Shoe.DoesNotExist:
            return JsonResponse({"message":"Shoe does not exist"})
    else:
        try:
            content=json.loads(request.body)
            shoe=Shoe.objects.get(id=id)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "SHoe does not exist"},
                status=400
            )
        Shoe.objects.filter(id=id).update(**content)
        shoes= Shoe.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False
        )











    #     try:
    #         shoe=Shoe.objects.get(id=id)
    #         shoe.delete()
    #         return JsonResponse(
    #             shoe,
    #             encoder=ShoeDetailEncoder,
    #             safe=False,
    #         )
    #     except Shoe.DoesNotExist:
    #         return JsonResponse({"message": "Cant delete shoe does not exist"})
    # else:
    #     try:
    #         content=json.loads(request.body)
    #         shoe=Shoe.objects.get(id=id)
    #         attributes=["manufacturer", "name", "color"]
    #         for attr in attributes:
    #             if attr in content:
    #                 setattr(shoe, attr, content[attr])
    #         shoe.save()
    #         return JsonResponse(
    #             shoe,
    #             encoder=ShoeDetailEncoder,
    #             safe=False
    #         )
    #     except Shoe.DoesNotExist:
    #         response-JsonResponse({"message": "Cant update"})
    #         response.status_code=404
    #         return response


# Create your views here.
