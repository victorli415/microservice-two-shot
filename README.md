# Wardrobify

Team:

* Person 1 - Robert Venegas
* Person 1 - Which microservice?
* Victor Li - Hats Microservice

## Design

## Shoes microservice

Create a shoes model, with name, manufacturer, style, and color fields
create views that use the models
using the correct url for the views

## Hats microservice
Hats model will include:
    name:charfield
    fabrics: charfield
    style name: charfield
    color: charfield
    picture_url: urlfield
    location: foreignKey to LocationVO

LocationVO model will include:
The properties from the Location class in the Wardrobe Model:
    closet_name: charfield
    section_number: integerfield
    sheld_number: integerfield
A new property that relates LocationVO model with Location From Wardrobe Model:
    import_href: charfield

Backend:
Hats will poll the location from wardrobe api at the base URL "http://wardrobe-api:8000/api/locations/". We will relate the Location href with that of the LocationVO href so we can share data between microservices. The poller gets location data from the Wardrobe api at intervals. With the data recieved from the poll, we can create new hat objects.

Frontend:
A single page application was rendered using React to show various properties from the hat/shoe model. On the main page, users can navigate between the list of shoes and hats. Navigating to the list page displays a list of the product and their properties. Here users can freely create shoes/hats depending on the page by clicking the "create link" and "delete" by clicking on the delete button. Clicking on the create link redirects to a "form" that allows users to input their desired specifications. To see the changes in the list view after CREATING and DELETING, users must refresh the page.

Wardrobify URLS
http://localhost:3000 - Main Page
http://localhost:3000/hats - List of hats with properties
http://localhost:3000/hats/new - Create hat form
