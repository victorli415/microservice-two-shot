from django.shortcuts import render
from .models import Hat, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'import_href',
        'closet_name',
        'section_number',
        'shelf_number',
        "id",
    ]

class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        'name',
        'style_name',
        'fabric',
        'color',
        'picture_url',
        'location',
        'id',

    ]

    encoders = {
        "location": LocationVODetailEncoder(),
    }




class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        'name',
        'style_name',
        'fabric',
        'color',
        'picture_url',
        'location',
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder=HatsListEncoder,
            safe=False
            )
    else:
        content = json.loads(request.body)
        print("content", content)
        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content['location'] = location
            print("location", location)
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_hats(request, pk):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted":count>0})
    else:
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
