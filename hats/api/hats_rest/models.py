from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


# Create your models here.
class Hat(models.Model):
    name = models.CharField(max_length=200, null=True)
    fabric = models.CharField(max_length=200, null=True)
    style_name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
    class Meta:
        ordering = ("name",'style_name', 'fabric', 'color', 'picture_url', 'location',)
