import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoeForm from './CreateShoeform';
import ShoeList from './ShoeList';

function App(props) {
  if (props.hats === undefined && props.shoes === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path='new' element={<HatForm />}/>
            <Route path='' element={<HatList hats={props.hats}/>}/>
          </Route>
          <Route path= "shoes">
            <Route path='new' element={<ShoeForm/>}/>
            <Route path='' element={<ShoeList shoes={props.shoes}/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
