import { Link } from "react-router-dom";

function ShoeList(props) {
    return (
        <>
        <Link to='/shoes/new'>Make a new Shoe</Link>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Picture</th>
                <th>Manufacturer</th>
                <th>Model name</th>
                <th>Color</th>
                <th>Closet</th>
                <th>Size</th>
                <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                return (
                    <tr key={shoe.id}>
                    <td><a href={shoe.picture_url}>
                        <img src={shoe.picture_url} className="img-thumbnail"  width="50px" height="50px"></img>
                    </a></td>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.model_name}</td>
                    <td>{ shoe.color }</td>
                    <td>{ shoe.bin.closet_name}</td>
                    <td>{ shoe.bin.bin_size}</td>
                    <td>
                        <button onClick={() => {
                            const url=`http://localhost:8080/api/shoes/${shoe.id}`;
                            fetch(url, {method:"DELETE"}).then(response => console.log(response.status))
                        }} className="btn btn-primary">DELETE</button>
                    </td>
                    </tr>
                );
                })}
            </tbody>
        </table>
        </>
    );
}

export default ShoeList;
