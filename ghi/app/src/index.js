import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// const root = ReactDOM.createRoot(document.getElementById('root'));
// async function loadHats() {
//   const response = await fetch('http://localhost:8090/api/hats/');
//   if (response.ok) {
//     const data = await response.json()
//     console.log("data",data.hats)
//     root.render(
//       <React.StrictMode>
//         <App hats={data.hats}/>
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response);
//   }
// }

// loadHats();



const root = ReactDOM.createRoot(document.getElementById('root'));
async function loadShoes() {
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatResponse = await fetch('http://localhost:8090/api/hats/');
  if (shoeResponse.ok && hatResponse.ok) {
    const shoeData = await shoeResponse.json()
    const hatData = await hatResponse.json()
    root.render(
      <React.StrictMode>
        <App shoes={shoeData.shoes} hats={hatData.hats}/>
      </React.StrictMode>
    );
  } else {
    console.error(shoeResponse);
    console.error(hatResponse);
  }
}

loadShoes();
