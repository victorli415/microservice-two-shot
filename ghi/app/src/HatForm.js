import React, { useState, useEffect } from 'react';

function HatForm () {
    const [closets, setClosets] = useState([])
    const [name, setName] = useState('')
    const [styleName, setstyleName] = useState('')
    const [fabric, setFabric] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setpictureUrl] = useState('')
    const [closet, setCloset] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value
        setstyleName(value)
    }
    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setpictureUrl(value)
    }
    const handleClosetChange = (event) => {
        const value = event.target.value
        setCloset(value)
    }

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/locations/')

        if (response.ok) {
            const data = await response.json();

            setClosets(data.locations)
        }
    }
    useEffect(() => {
        fetchData();
      }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name;
        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = `/api/locations/${closet}/`;
        console.log("data", data)

        const url = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {

            setName('');
            setstyleName('');
            setFabric('');
            setColor('');
            setpictureUrl('');
            setCloset('');
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form id="create-hat-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control" onChange={handleNameChange}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Style Name" value={styleName} required type="text" name="style_name" id="style_name" className="form-control" onChange={handleStyleNameChange}/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Fabric" value={fabric} required type="text" name="fabric" id="fabric" className="form-control" onChange={handleFabricChange}/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Color" value={color} required type="text" name="color" id="color" className="form-control" onChange={handleColorChange}/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Picture Url" value={pictureUrl} required type="url" name="picture_url" id="picture_url" className="form-control" onChange={handlePictureUrlChange}/>
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select value={closet}required name="closet_name" id="closet_name" className="form-select" onChange={handleClosetChange}>
                  <option value="">Choose a closest</option>
                  {closets.map(closet => {
                    return (
                    <option key={closet.id} value={closet.id}>
                        {closet.closet_name}
                    </option>
                    )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default HatForm;
